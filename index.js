const { Plugin } = require('powercord/entities');
const { getModule, React, getAllModules, FluxDispatcher } = require('powercord/webpack');
const { inject, uninject } = require('powercord/injector');

const powercord = require('powercord/');

module.exports = class LunaCensor extends Plugin {
    async startPlugin() {
        const blockedWords = [
            "ok", "k", "okay"
        ];

        // The replaceents for the word, add more here.
        const replaceents = ["alright", "sure", "nice"];

        inject("lunaFetchingTest", FluxDispatcher, "dispatch", (args) => {
            if (args[0].type == "MESSAGE_CREATE" && args[0].message && args[0].message.content) {
                try {
                    // console.log(args[0].message.id + " - " + args[0].message.content);
                    var messageSplit = args[0].message.content.split(/\s+/);
                    var messageRebuilt = "";
    
                    for (var word in messageSplit) {
                        if (blockedWords.indexOf(messageSplit[word].toLowerCase()) > -1) {
                            console.log("[LunaCensor] " + args[0].message.content);
                            messageRebuilt += replaceents[Math.floor(Math.random() * replaceents.length)] + " ";
                        }

                        else messageRebuilt += messageSplit[word] + " ";
                    }
    
                    args[0].message.content = messageRebuilt;
                } catch (error) {
                    console.error(error);
                }
            }

            if (args[0].type == "MESSAGE_UPDATE" && args[0].message && args[0].message.content) {
                try {
                    var messageSplit = args[0].message.content.split(/\s+/);
                    var messageRebuilt = "";
    
                    for (var word in messageSplit) {
                        if (blockedWords.indexOf(messageSplit[word].toLowerCase()) > -1) {
                            console.log("[LunaCensor] " + args[0].message.content);
                            messageRebuilt += replaceents[Math.floor(Math.random() * replaceents.length)] + " ";
                        }
                        else messageRebuilt += messageSplit[word] + " ";
                    }
    
                    args[0].message.content = messageRebuilt;
                } catch (error) {
                    console.error(error);
                }
            }

            return args;
        }, true);
    };

    pluginWillUnload() {
        uninject("lunaFetchTest");
        uninject("lunaRecieve");
    }
};